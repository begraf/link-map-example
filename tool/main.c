#include <dynamic_A.h>
#include <dynamic_B.h>
#include <static_lib.h>

#include <stdio.h>

int main(void)
{
    printf("magic(dynamic1): %d\n", dynamic_a_magic());
    printf("magic(dynamic2): %d\n", dynamic_b_magic());
    printf("magic(static  ): %d\n", static_lib_magic());
}